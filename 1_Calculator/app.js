
const prompt = require('prompt');

var number1, number2, number, next_number,operation;
var total = 0;

askOperation();

function askOperation() {

    console.log("Choose your operation. 1. Add  2. Subtract 3. Divide 4. Multiply 5. Exit\n");
    prompt.get(['operation'], function (err, result) {

        switch (result.operation) {
            case '1': operation = 'add'; break;
            case '2': operation = 'subtract'; break;
            case '3': operation = 'divide'; break;
            case '4': operation = 'multiply'; break;
            case '5': console.log("The result is: ", total);
                      total = 0;
                      process.exit(0);
        }
        if (total == 0) {
            getFirstTwoNumbers();
        }
        else {
            getNextNumbers();
        }
    });
}

function getFirstTwoNumbers() {

    console.log("Enter First Number and Second Number");
    prompt.get(['number1', 'number2'], function (err, result) {

        number1 = result.number1;
        number2 = result.number2;

        if (operation == 'add') {
            total = parseInt(number1) + parseInt(number2);
        }
        else if (operation == 'subtract') {
            total = parseInt(number1) - parseInt(number2);
        }
        else if (operation == 'divide') {
            total = parseInt(number1) / parseInt(number2);
        }
        else {
            total = parseInt(number1) * parseInt(number2);
        }

        confirmNewNumber();

    });
}

function confirmNewNumber() {
    console.log("Do you want to enter a new number: y/n");
    prompt.get(['next_number'], function (err, result) {
        console.log(result.next_number);
        if (result.next_number == 'y') {
            getNextNumbers();
        }
        else {
            console.log("The result is: ", total);
            askOperation();
        }
    });
}

function getNextNumbers() {
    console.log("total",total);
    console.log("Enter Number");
    prompt.get(['number'], function (err, result) {

        number = result.number;
        console.log(operation);

        if (operation == 'add') {
            total = total +  parseInt(number);
        }
        else if (operation == 'subtract') {
            total = total - parseInt(number);
        }
        else if (operation == 'divide') {
            total = total / parseInt(number);
        }
        else {
            total = total * parseInt(number);
        }

        confirmNewNumber();
    });
}
