const sqlite3 = require('sqlite3').verbose();

var db;

var setUpDatabase = () => {
    db  = new sqlite3.Database('book.db', (err) => {
        if (err) {
            return console.error(err.message);
        }
    });
    db.run('CREATE TABLE IF NOT EXISTS books(title TEXT, author TEXT)');
}

var closeDbConnection = () => {
    db.close();
}

module.exports = {
    setUpDatabase,
    closeDbConnection
}