
const fs = require('fs');

var addBook = (title, author) => {
    var sql = `INSERT INTO books VALUES(NULL,${title},${author})`;

    var stmt = db.prepare(`INSERT into books values(?, ?);`);
    stmt.run(title,author);
}


var listBook = (getBookArray) => {
    db.all("SELECT rowid,title,author from books",function(err,rows){
        return getBookArray(rows);
    });
}

var deleteBook = (id) => {

    db.run(`DELETE FROM books WHERE rowid=?`, id, function(err) {
        if (err) {
          return console.error(err.message);
        }
    });
    
}


module.exports = {
    addBook,
    listBook,
    deleteBook
}