const book = require('./book.js');
const databaseConnection = require('./database.js');
var express = require("express");
var app = express();
var router = express.Router();
var bodyParser = require('body-parser')
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

// app.use(express.json());       // to support JSON-encoded bodies
// app.use(express.urlencoded()); // to support URL-encoded bodies


databaseConnection.setUpDatabase();

var path = require('path');


app.use("/", express.static(__dirname + '/'));
app.set('view engine', 'ejs');
app.get('/', function(req, res) {

var list = book.listBook(getBookArray => {
    console.log(getBookArray);
    if(getBookArray) {
        res.render('list.ejs', {data:getBookArray} );
    }
});
});

app.get('/delete/:id', function(req,res) {
    book.deleteBook(req.params.id);
    res.redirect('/');
});

app.get('/add',function(req,res) {
    res.render('add.ejs');
    // console.log(res.body);
});

app.post('/add',function(req,res) {
    console.log('post body',req.body);
    book.addBook(req.body.title,req.body.author);
    res.redirect('/');
});


app.listen(8080);
