const prompt = require('prompt');
var number1, number2, number, next_number, operator;
var total = 0;

var askOperation = () => {

    console.log("Choose your operation. 1. Add  2. Subtract 3. Divide 4. Multiply 5. Exit\n");
    prompt.get(['operation'], function (err, result) {
        
        switch (result.operation) {
            case '1': operator = 'add'; break;
            case '2': operator = 'subtract'; break;
            case '3': operator = 'divide'; break;
            case '4': operator = 'multiply'; break;
            case '5': console.log("The result is: ", total);
                      total = 0;
                      process.exit(0);
        }
        if (total == 0) {
            getFirstTwoNumbers();
        }
        else {
            getNextNumbers();
        }
    });
}

var getFirstTwoNumbers = () => {

    console.log('operator',operator);
    console.log("Enter First Number and Second Number");
    prompt.get(['number1', 'number2'], function (err, result) {

        number1 = result.number1;
        number2 = result.number2;

        switch(operator) {
            case 'add' :  total = parseInt(number1) + parseInt(number2); break;
            case 'subtract' : total = parseInt(number1) - parseInt(number2); break;
            case 'divide' : total = parseInt(number1) / parseInt(number2); break;
            case 'multiply' : total = parseInt(number1) * parseInt(number2);
        }

        confrimNewNumber();

    });
}

var confrimNewNumber = () => {
    console.log("Do you want to enter a new number: y/n");
    prompt.get(['next_number'], function (err, result) {
        console.log(result.next_number);
        if (result.next_number == 'y') {
            getNextNumbers();
        }
        else {
            console.log("The result is: ", total);
            askOperation();
        }
    });
}

var getNextNumbers = () =>  {
    console.log("total",total);
    console.log("Enter Number");
    prompt.get(['number'], function (err, result) {

        number = result.number;
        console.log(operator);

        switch(operator) {
            case 'add' : total = total +  parseInt(number); break;
            case 'subtract' : total = total - parseInt(number); break;
            case 'divide' : total = total / parseInt(number); break;
            case 'multiply' : total = total * parseInt(number);
        }

        confrimNewNumber();
    });
}

module.exports = {
    askOperation
}