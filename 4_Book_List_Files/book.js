const fs = require('fs');

addBook = (id,title,author,callback) => {
    var book = id+','+title+','+author+'\n';
    fs.appendFileSync('bookList.txt',book,'utf8');
    return callback('true')
}

writeFile = (bookArray,callback) => {
    console.log(bookArray);
   for(i in bookArray) {
     if(i == 0) {
        fs.writeFileSync('bookList.txt',bookArray[i]+'\n','utf8');
     }
     else{
        fs.appendFileSync('bookList.txt',bookArray[i]+'\n','utf8');
     }

   }
}

listBook = () => {
    var bookList = fs.readFileSync('bookList.txt','utf8');
    var bookArray = bookList.trim().split('\n').filter( book => book.length !== 0 );
    return bookArray;
}

deleteBook = (id,callback) => {
    var bookArray = listBook();
    bookArray.splice(id,1);
    writeFile(bookArray);
    return callback('true');
}

displayBook = (callback) => {
    try {
        var bookArray = listBook();
        bookArray.forEach( (value, i) => {
         console.log(`[${i}]  ===>  ${value}`);
        });
    }catch (e) {
        console.log("No Books");
    }
    return callback('true');
}   

module.exports = {
    addBook,
    listBook,
    deleteBook,
    displayBook
}