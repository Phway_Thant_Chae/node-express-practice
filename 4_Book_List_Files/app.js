const book = require('./book.js');
const yargs = require('yargs');
const prompt = require('prompt');


getPrompt();


function getPrompt() {
    console.log('-----------------------------------------');
    console.log('1. Add Book 2. List Books 3. Delete Book');
    console.log('-----------------------------------------');
    prompt.get(['book'], function(err,result) {
        console.log('++++++++++++++++++++++++++++++++++++++');
        switch(result.book) {
            case'1' : addBook(); break;
            case '2' : listBook(); break;
            case '3' : deleteBook(); break;
            default  : process.exit(0);
        }
    });
}


function addBook() {
    prompt.get(['id','title','author'], function(err,result) {
        book.addBook(result.id,result.title,result.author, callback => {
            if(callback == 'true') {
                getPrompt();
            }
        });
    });
}

function listBook() {
    console.log("Book List");
    console.log("-------------------------------");
    book.displayBook(callback => {
        if(callback == 'true') {
            getPrompt();
        }
    });
}

function deleteBook() {
    console.log("Book List");
    console.log("-------------------------------");
    book.displayBook(callback => {});
    prompt.get(['id'], function(err,result) {
        book.deleteBook(result.id,callback => {
            if(callback == 'true') {
                getPrompt();
            }
        });
    })
}