
const fs = require('fs');
const sqlite3 = require('sqlite3').verbose();

var db;

var setUpDatabase = () => {
    db  = new sqlite3.Database('book.db', (err) => {
        if (err) {
            return console.error(err.message);
        }
    });
    db.run('CREATE TABLE IF NOT EXISTS books(title TEXT, author TEXT)');
}

var closeDbConnection = () => {
    db.close();
}

var addBook = (title, author, callback) => {
    var sql = `INSERT INTO books VALUES(NULL,${title},${author})`;

    var stmt = db.prepare(`INSERT into books values(?, ?);`);
    stmt.run(title,author);
    return callback('true');
}


listBook = (getBookArray) => {
    db.all("SELECT rowid,title,author from books",function(err,rows){
        return getBookArray(rows);
    });
}

deleteBook = (id, callback) => {
    // var stmt = db.prepare(`DELETE FROM books WHERE rowid=?`);
    // stmt.run(id);
    db.run(`DELETE FROM books WHERE rowid=?`, id, function(err) {
        if (err) {
          return console.error(err.message);
        }
        return callback('true');
    });
    
}

displayBook = (callback) => {
    try {
        listBook(getBookArray => {
                  getBookArray.forEach((value, i) => {
                    console.log(`[${value.rowid}]  ===>  ${value.title},${value.author}`);
                });
            return callback('true');
        });
  
    } catch (e) {
        console.log("No Books");
    }
   
}

module.exports = {
    addBook,
    listBook,
    deleteBook,
    displayBook,
    setUpDatabase,
    closeDbConnection
}