const book = require('./book.js');
const prompt = require('prompt');


book.setUpDatabase();
getPrompt();

function getPrompt() {
    console.log('-----------------------------------------');
    console.log('1. Add Book 2. List Books 3. Delete Book');
    console.log('-----------------------------------------');
    prompt.get(['book'], function (err, result) {
        console.log('++++++++++++++++++++++++++++++++++++++');
        switch (result.book) {
            case '1': addBook(); break;
            case '2': listBook(); break;
            case '3': deleteBook(); break;
            default: book.closeDbConnection(); process.exit(0);
        }
    });
}


function addBook() {
    prompt.get(['title', 'author'], function (err, result) {
        book.addBook(result.title, result.author, callback => {
            if (callback == 'true') {
                getPrompt();
            }
        });
    });
}

function listBook() {
    console.log("Book List");
    console.log("-------------------------------");
    book.displayBook(callback => {
        if (callback == 'true') {
            getPrompt();
        }
    });
}

function deleteBook() {
    console.log("Book List");
    console.log("-------------------------------");
    book.displayBook(callback => {
        if (callback == 'true') {
            prompt.get(['id'], function (err, result) {
                book.deleteBook(result.id, callback => {
                    if (callback == 'true') {
                        getPrompt();
                    }
                });
            });
        }
    });

}